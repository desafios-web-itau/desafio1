package br.com.itau;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import br.com.itau.modelo.Lancamento;
import br.com.itau.service.LancamentoService;

public class Exercicio {
	
	private static int categoria = 0;
	private static int mes = 0;
	private static double total = 0;
	protected static Scanner sc = new Scanner(System.in);
	

	public static void main(String[] args) {
		List<Lancamento> lancamentos = new LancamentoService().listarLancamentos();
		
		//TODO: continue daqui		
		
		Comparator<Lancamento> comparaMes = new Comparator<Lancamento>() {
			public int compare(Lancamento l1, Lancamento l2) {				
				return l1.getMes().compareTo(l2.getMes());
			}
		};		
		
		Collections.sort(lancamentos, comparaMes);
		System.out.println("Lan�amentos ordenados por m�s:\n");
		lancamentos.forEach(System.out::println);
		
		do {
			System.out.println("\nEscolha a categoria desejada para ver os lan�amentos [1-6]:");
			categoria = sc.nextInt();
		} while(categoria <= 0 || categoria > 6);
		lancamentos.stream()
			.filter(l -> l.getCategoria() == categoria)
			.forEach(System.out::println);
		
		do {
			System.out.println("\nEscolha o m�s para ver o valor da fatura [1-12]:");
			mes = sc.nextInt();
		} while(mes <= 0 || mes > 12);
		lancamentos.stream()
		.filter(l -> l.getMes() == mes)
		.forEach(c -> total += c.getValor());
		System.out.printf("Valor da fatura para o m�s %d � R$ %.2f", mes, total);
	}
}
